<div align="center">
<h1>ZFVM</h1>
<a href="https://codeberg.org/Zillowe/ZFOL">
  <img
    alt="ZFOL-1.0"
    src="https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/dark.svg"
  />
</a>
<hr/>
</div>

## Zillowe Foundation Versioning Method

ZFVM looks like this:

[Branch] [Status] [X.Y.Z]

| Branch                                            | Status                                                  | X            | Y            | Z                                  |
| ------------------------------------------------- | ------------------------------------------------------- | ------------ | ------------ | ---------------------------------- |
| The branch you are developing in, e.g. Production | The status of the code, Alpha, Pre-Alpha, Release, etc. | Major update | Minor update | Bug fixes, some small enhancements |

X: is a major update, a big change, like re-branding, or a bunch of big features.
Y: is a minor update, a small change, a new feature.
Z: is a bug fix, a very small change, or a small enhancement.

When you make a change, you increment the Z number.
When you make a new feature, you increment the Y number.

When you make a big change, you increment the X number.
When Y increased, the Z number is reset to 0.
When X increased, the Y number is reset to 0, and the Z number is reset to 0.

### Branches and Statuses

### List

List of all branches and statuses.

| Branches: | Production | Development | Special  | Public | ---         | ---          | ---  | ---     |
| --------- | ---------- | ----------- | -------- | ------ | ----------- | ------------ | ---- | ------- |
| Statuses: | Pre-Alpha  | Alpha       | Pre-Beta | Beta   | Pre-Release | Early-Access | Demo | Release |

The statues are arranged in order of development.
The higher the status, the closer the code is to release.

#### Information

- Branches:
  - Production or Prod.: is the code that is used to deploy the product.
  - Development or Dev.: is the code that is used to develop the product.
  - Special or Spec.: is the code that is used for special purposes (e.g. for testing new features, ).
  - Public or Pub.: is the code that is still in development, but deployed to the public to try it out.
- Statuses:
  - Pre-Alpha: is the code that is in the early early stages of development.
  - Alpha: is the code that is in the early stages of development.
  - Pre-Beta: is the code that is in the middle stages of development.
  - Beta: is the code that is in the late stages of development.
  - Pre-Release: is the code that is ready to be released, but not yet released.
  - Early-Access: is the code that is released to the public to try it out.
  - Demo: is the code that is released to the public to try it out.
  - Release: is the code that is deployed to the production.

### Example

- Dev. Pre-Alpha 4.3.5
- Prod. Release 5.6.1
